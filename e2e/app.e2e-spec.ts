import { CgmPage } from './app.po';

describe('cgm App', function() {
  let page: CgmPage;

  beforeEach(() => {
    page = new CgmPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
