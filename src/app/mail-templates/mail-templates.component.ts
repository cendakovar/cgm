import {Component, OnInit, ViewContainerRef} from '@angular/core';
import {MdIconRegistry} from "@angular/material";
// import {LocalStorage, StorageProperty} from 'h5webstorage';
import { Overlay } from "angular2-modal";
import { Modal } from 'angular2-modal/plugins/bootstrap';

@Component({
  selector: 'app-mail-templates',
  templateUrl: './mail-templates.component.html',
  styleUrls: ['./mail-templates.component.css']
})
export class MailTemplatesComponent implements OnInit {

  templates: any[]; // array of Objects - [{...}, {...}];
  mailTemplate: any; //Object - {};

  // @StorageProperty() public SomeValue: string = 'Init text...';

  config = {
    defaultSortOrder: 'title',
    allowedSortByProperties: ['title', 'id'],
    testingTemplates: JSON.stringify([
      {
        'id': 1,
        'title': 'Template 1',
        'content': '<h1>Hello world</h1><p>lorem ipsum dolor sit amet</p>'
      }, {
        'id': 2,
        'title': 'Custom template 2',
        'content': `<h1>Nam egestas eros sed dapibus tempus</h1><p>Cras vel risus blandit, egestas nunc in, eleifend felis. Morbi sed facilisis ipsum, non vehicula nibh. Aenean quis orci elementum, placerat nunc rutrum, aliquam nulla. Mauris ac odio at lorem malesuada finibus eu sed tellus. Curabitur nisi diam, dapibus ac lobortis sit amet, auctor at est. Nunc a lectus eget urna fringilla sagittis eget eu nibh. Maecenas felis quam, blandit vitae orci sed, ullamcorper convallis leo. Nullam tristique vitae nibh et pulvinar. Praesent id orci eget urna sollicitudin porta. Ut non nibh nisi. Mauris eu rhoncus massa, quis suscipit augue.</p><h2>Duis odio lacus</h2><ul><li>item 1</li><li>item 2</li><li>item 3</li></ul>`
      }, {
        'id': 3,
        'title': 'Another template',
        'content': '<h1>Aenean diam justo, sollicitudin sed ligula vel</h1><p>This is <strong>testing</strong> template</p><ul><li>item 1</li><li>item 2</li><li>item 3</li></ul>'
      }, {
        'id': 4,
        'title': 'Last template',
        'content': `<h1>Nam egestas eros sed dapibus tempus</h1><ul><li>item 1</li><li>item 2</li><li>item 3</li></ul><h2>Duis odio lacus</h2><p>Cras vel risus blandit, egestas nunc in, eleifend felis. Morbi sed facilisis ipsum, non vehicula nibh. Aenean quis orci elementum, placerat nunc rutrum, aliquam nulla. Mauris ac odio at lorem malesuada finibus eu sed tellus. Curabitur nisi diam, dapibus ac lobortis sit amet, auctor at est. Nunc a lectus eget urna fringilla sagittis eget eu nibh. Maecenas felis quam, blandit vitae orci sed, ullamcorper convallis leo. Nullam tristique vitae nibh et pulvinar. Praesent id orci eget urna sollicitudin porta. Ut non nibh nisi. Mauris eu rhoncus massa, quis suscipit augue.</p>`
      }
    ])
  };

  currentSortOrder = this.config.defaultSortOrder;

  constructor(
    mdIconRegistry: MdIconRegistry,
    // private localStorage: LocalStorage,
    overlay: Overlay,
    vcRef: ViewContainerRef,
    public modal: Modal
  ) {
    this.prepareTestingTemplates();

    overlay.defaultViewContainer = vcRef;

    mdIconRegistry
      .registerFontClassAlias('fontawesome', 'fa');
  }

  updateTemplatesInLocalStorage(dataToStore = null) {
    localStorage.setItem('templates', dataToStore || JSON.stringify(this.templates));

    this.templates = this.getTemplates();
  }

  prepareTestingTemplates() {
    this.templates = this.getTemplates();

    if(typeof this.templates === typeof [] && this.templates.length === 0) {
      // localStorage.setItem('templates', this.config.testingTemplates);
      this.updateTemplatesInLocalStorage(this.config.testingTemplates);
    }
  }

  sortTemplates(sortOrder = this.currentSortOrder) {
    let sortedTemplates = this.templates.sort(function(a,b) {

      if(sortOrder === 'id') {
        return (a[sortOrder] > b[sortOrder]) ? 1 : ((b[sortOrder] > a[sortOrder]) ? -1 : 0);
      }

      return (a[sortOrder].toLowerCase() > b[sortOrder].toLowerCase()) ? 1 : ((b[sortOrder].toLowerCase() > a[sortOrder].toLowerCase()) ? -1 : 0);
    });

    this.currentSortOrder = sortOrder;
    this.templates = sortedTemplates;
    // this.localStorage.setItem('SomeValue', sortOrder);

    return sortedTemplates;
  }


  getTemplates() {
    this.templates = JSON.parse(localStorage.getItem('templates'));
    if(this.templates && this.templates.length > 0) {
      return this.sortTemplates();
    }

    return [];
  }

  getSingleTemplate(templateID = 0) {
    let result = this.templates.find( templateObject => templateObject.id == templateID);
    this.mailTemplate = result;
  }

  prepareEmptyTemplate() {
    this.mailTemplate = {
      'title': '',
      'content': ''
    };
  }

  handleEventsFromChild(event) {

    switch(event) {
      case 'create':
        this.createTemplate(this.mailTemplate);
        break;

      case 'clear':
        this.clearTemplate(this.mailTemplate);
        break;

      case 'update':
        this.updateTemplate(this.mailTemplate);
        break;

      case 'delete':
        this.deleteTemplate(this.mailTemplate);
        break;
    }

  }

  createTemplate(data) { console.log('CREATE NEW TEMPLATE');
    data.id = this.getNextTemplateId();

    this.mailTemplate = data;

    this.templates.push(this.mailTemplate);

    // localStorage.setItem('templates', JSON.stringify(this.templates));
    this.updateTemplatesInLocalStorage();

    this.modal.alert()
      .size('lg')
      .dialogClass('bottom-right-corner')
      .isBlocking(false)
      .showClose(true)
      .keyboard(27)
      .title('Template has been created')
      .message(`Template with title "${data.title}" hase been created. Good job.`)
      .okBtn('OK')
      .open()
      .then(dialog => {
        document.getElementsByTagName('body')[0].classList.add('alert-autoclose-opened');


        setTimeout(() => {
          dialog.destroy();
          document.getElementsByTagName('body')[0].classList.remove('alert-autoclose-opened');
        }, 3000)
      })
      .catch(err => console.log("Error has been spotted: ", err)); // if were here it was cancelled (click or non block click)

  }

  clearTemplate(data) {
    this.prepareEmptyTemplate();
  }

  updateTemplate(data) { console.log('UPDATE TEMPLATE');

    let templateToBeUpdated = this.templates.find(t => t.id === data.id);
    let indexOfTemplateToBeUpdated = this.templates.indexOf(templateToBeUpdated);

    if (indexOfTemplateToBeUpdated > -1) {
      this.templates[indexOfTemplateToBeUpdated].id = data.id;
      this.templates[indexOfTemplateToBeUpdated].title = data.title;
      this.templates[indexOfTemplateToBeUpdated].content = data.content;
      this.updateTemplatesInLocalStorage();
    }

    this.updateTemplatesInLocalStorage();

    this.modal.alert()
      .size('lg')
      .dialogClass('bottom-right-corner')
      .isBlocking(false)
      .showClose(true)
      .keyboard(27)
      .title('Template has been updated')
      .message(`Template with title "${templateToBeUpdated.title}" hase been updated. Good job.`)
      .okBtn('OK')
      .open()
      .then(dialog => {
        document.getElementsByTagName('body')[0].classList.add('alert-autoclose-opened');


        setTimeout(() => {
          dialog.destroy();
          document.getElementsByTagName('body')[0].classList.remove('alert-autoclose-opened');
        }, 3000)
      })
      .catch(err => console.log("Error has been spotted: ", err)); // if were here it was cancelled (click or non block click)
  }

  deleteTemplate(data) { console.log('DELETE TEMPLATE');
    this.modal.confirm()
      .size('lg')
      .isBlocking(false)
      .showClose(true)
      .keyboard(27)
      .title('Confirm deleting of template')
      .body('Please confirm that you know what are you doing, right now :)')
      .okBtn('Yep, sure')
      .cancelBtn('Aww snap, go back!')
      .open()
      .then(dialog => dialog.result) // dialog has more properties,lets just return the promise for a result.
      .then(result => {
        if(!result) {
          return;
        }

        let templateToBeDeleted = this.templates.find(t => t.id === data.id);
        let indexOfTemplateToBeDeleted = this.templates.indexOf(templateToBeDeleted);

        if (indexOfTemplateToBeDeleted > -1) {
          this.templates.splice(indexOfTemplateToBeDeleted, 1);
          this.updateTemplatesInLocalStorage();
          this.prepareEmptyTemplate();
        }
      }) // if were here ok was clicked.
      .catch(err => console.log("CANCELED")); // if were here it was cancelled (click or non block click)

    return;
  }

  getNextTemplateId() {
    let highKey = 0;

    for(let t of this.templates) {
      highKey = Math.max(highKey, t.id);
    }

    return ++highKey;
  }

  openStart() { console.log('Open Started'); document.getElementById('showSidebar').style.display = 'none'; }
  open() { console.log('Open Finished'); }

  closeStart() { console.log('Close Started'); document.getElementById('showSidebar').style.display = 'inline-block'; }
  close() { console.log('Close Finished'); }

  ngOnInit() {
    this.getSingleTemplate();
    this.prepareEmptyTemplate();
  }
}
