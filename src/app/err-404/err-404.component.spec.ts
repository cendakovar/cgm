/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { err404Component } from './err-404.component';

describe('404Component', () => {
  let component: err404Component;
  let fixture: ComponentFixture<err404Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ err404Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(err404Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
