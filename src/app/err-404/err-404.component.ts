import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-err-404',
  templateUrl: './err-404.component.html',
  styleUrls: ['./err-404.component.css']
})
export class err404Component implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  stepBack() {
    history.back();
  }

}
