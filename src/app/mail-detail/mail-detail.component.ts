import {Component, ElementRef, EventEmitter, Input, OnChanges, OnInit, Output, ViewChild} from '@angular/core';


@Component({
  selector: 'app-mail-detail',
  templateUrl: './mail-detail.component.html',
  styleUrls: ['./mail-detail.component.css']
})
export class MailDetailComponent implements OnInit, OnChanges {

  @ViewChild('mailTemplateId') mailTemplateId:ElementRef;
  @ViewChild('mailTemplateTitle') mailTemplateTitle:ElementRef;
  @ViewChild('mailTemplateContent') mailTemplateContent:ElementRef;

  @Output()
  handleDataInParentClass:EventEmitter<string> = new EventEmitter();

  // @Output()
  // dataForNewTemplate: any;

  sendEventToParent(type) {
    //console.log('MAIL DETAIL: ', this.template, this.mailTemplateContent.nativeElement.value);

    switch(type) {
      case 'create':
        this.handleDataInParentClass.emit('create');
        break;

      case 'clear':
        this.handleDataInParentClass.emit('clear');
        break;

      case 'update':
        console.log(this.template);
        // this.mailTemplate = this.template;
        this.handleDataInParentClass.emit('update');
        break;

      case 'delete':
        this.handleDataInParentClass.emit('delete');
        break;
    }
  }

  @Input() template;

  // mailTemplate = {};

  // mailTemplate = this.template || {
  //   id: null,
  //   title: 'Sample template (not in DB)',
  //   content: `
  //     <h1>Dummy template</h1>
  //     <p>Lorem ipsum dolor sit amet</p>
  //   `
  // };

  constructor() {

  }

  ngOnInit() {
  }

  ngOnChanges() {
    // console.log('change');

    // this.mailTemplate = this.template || {'title': '', 'content': ''};
  }

}
