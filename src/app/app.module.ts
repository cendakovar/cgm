import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { MaterialModule } from '@angular/material';
import { ModalModule } from 'angular2-modal';
import { BootstrapModalModule } from 'angular2-modal/plugins/bootstrap';

import { AppComponent } from './app.component';
import { AuthService } from "./auth.service";
import { LoginFormComponent } from './login-form/login-form.component';
import { PrivateComponent } from './private/private.component';
import { Routes, RouterModule } from "@angular/router";
import { MailTemplatesComponent } from './mail-templates/mail-templates.component';
import { MailDetailComponent } from './mail-detail/mail-detail.component';
// import { WebStorageModule, BROWSER_STORAGE_PROVIDERS } from "h5webstorage";
import { err404Component } from './err-404/err-404.component';

const appRoutes: Routes = [
  { path: 'home', component: PrivateComponent},
  { path: 'login', component: LoginFormComponent },
  { path: 'logout', component: LoginFormComponent },
  { path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  // TODO: 404
  { path: '**', component: err404Component }
];

@NgModule({
  declarations: [
    AppComponent,
    LoginFormComponent,
    PrivateComponent,
    MailTemplatesComponent,
    MailDetailComponent,
    err404Component
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes),
    MaterialModule,
    ModalModule.forRoot(),
    BootstrapModalModule,
    // WebStorageModule
  ],
  providers: [
    AuthService,
    // BROWSER_STORAGE_PROVIDERS
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
