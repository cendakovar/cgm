import { Component, OnInit } from '@angular/core';
import {AuthService} from "../auth.service";

@Component({
  selector: 'app-login-form',
  providers: [AuthService],
  templateUrl: './private.component.html',
  styleUrls: ['./private.component.css']
})
export class PrivateComponent implements OnInit {

  constructor(private _auth: AuthService) {
    if(this._auth.checkCredentials()) {
      let username = JSON.parse(localStorage.getItem('user')).username;
      console.log(`User ${username} is logged`);
    }
  }

  ngOnInit() {

  }

  logout(event) {
    event.preventDefault();
    this._auth.logout();
  }

}
