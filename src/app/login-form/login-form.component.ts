import { Component, OnInit } from '@angular/core';
import { AuthService, User } from "../auth.service";

@Component({
  selector: 'app-login-form',
  providers: [AuthService],
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {

  public user = new User('','');
  public errorMsg = '';

  constructor(private _auth: AuthService) {
    // TODO: doplnit zpravu po presmerovani z /home pokud neni uzivatel prihlaseny
    this.errorMsg = this._auth.message;
    this._auth.logout();
  }

  // onClick(evt, username, password) {
  //   console.log(evt.type);
  //
  //   console.log(`Username: ${username}, Password: ${password}`);
  // }

  login() {
    if(!this._auth.login(this.user)){
      this.errorMsg = this._auth.message;
    }
  }

  ngOnInit() {
  }

}
