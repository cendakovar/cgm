import { Injectable } from '@angular/core';
import {Router} from "@angular/router";

export class User {
  constructor(
    public username: string,
    public password: string) { }
}

let users = [
  new User('admin','admin'),
  new User('user','user')
];

@Injectable()
export class AuthService {

  message = '';

  constructor(
    private _router: Router){}

  logout() {
    localStorage.removeItem("user");
    this.message = 'User logged out';
    console.log(this.message);

    this._router.navigate(['/login']);
  }

  login(user){
    let authenticatedUser = users.find(u => u.username === user.username);
    if (authenticatedUser && authenticatedUser.password === user.password){
      localStorage.setItem("user", JSON.stringify(authenticatedUser));
      this.message = 'User logged in';
      console.log(this.message);

      this._router.navigate(['/home']);
      return true;
    }

    this.message = 'User authentication failed';
    return false;

  }

  checkCredentials(){
    if (localStorage.getItem("user") === null){
      this.message = 'Please log in first before viewing this content!';
      this._router.navigate(['/login']);
      return false;
    }

    return true;
  }
}
